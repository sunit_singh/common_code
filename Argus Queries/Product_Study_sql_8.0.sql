--- Study Data
Select  Lpr.Protocol_Desc Project_Id, Decode(Lpr.Display,1,'Yes','No') Display, Ls.Study_Num Study_Id , Lsc.Blind_Name Study_Name, Lst.Study_Type,
Lpr.Protocol_Desc Project_Id, Ls.Other_Num Other_Id, Decode(Ls.Template,0,'No','Yes') Template_Only, Lcc.Description Observe_Study_Type_E2b, Ldp.Dev_Phase Study_Development_Phase
,Lp.Prod_Name Product_Name, Lcrt.Ref_Type_Desc Reference_Type,
 Lc.Country Country ,
  Lsr.Reference Reference_Number,
ll.Trade_name Product_License, lp.prod_name_abbrv Product_Abbreviation, lce.center_name Center_Name, to_char(ls.study_desc) Study_Description, 
COND_NAME Investigator_Alert, decode(UNBLIND_OK,0,'No','Yes') Study_eligible_for_Unblinding , decode(ls.ENCODING,0,'No','Yes') Enable_Study_Specific_Encoding,
lsco.inactive_date End_Date, (select country from lm_countries lc1, lm_study_countries lsc1 where lc1.country_id=lsc1.country_id and lsc1.study_key=lsc.study_key) Selected_Countries,
decode(ls.template,0,'No','Yes') Inherit_rules_temp, ls.comments COMMENTS
From cfg_adv_cond cac,
Lm_Lic_Products Llp, Lm_License Ll,Lm_Clinical_Ref_Types Lcrt, Lm_Study_References Lsr, 
Lm_Protocols Lpr, Lm_Studies Ls, 
Lm_Study_Cohorts Lsc, Lm_Study_Types Lst, Lm_Case_Classification Lcc, 
Lm_Dev_Phase Ldp, Lm_Study_Products Lsp, Lm_Product Lp, Lm_Countries Lc, 
Lm_Centers lce, lm_study_centers lsce ,lm_study_countries lsco
Where lsco.study_key(+)=ls.study_key and cac.ac_id (+) =ls.ALERT_AC_ID and 
lce.center_id (+) = lsce.center_id and lsce.study_key(+)  = ls.study_key and
llp.product_id (+) = lp.product_id and ll.license_id (+) =  llp.license_id and 
Lsr.STUDY_KEY (+) =Ls.STUDY_KEY and
lc.country_id (+) =lsr.country_id and 
lcrt.ref_type_id (+)=lsr.ref_type_id and 
lp.product_id (+)= lsp.product_id and lsp.cohort_id (+)=lsc.cohort_id and Lcc.Classification_Id(+) =Ls.Classification_Id And  
lst.study_type_id(+) =lsc.study_type_id and lsc.study_key(+) = ls.study_key and Lpr.Protocol_Id (+)=ls.id_protocol 
and ldp.dev_phase_id(+)= ls.dev_phase_id and cac.deleted is null and Llp.deleted is null and ll.deleted is null and lcrt.deleted is null and lsr.deleted is null
and lpr.deleted is null and ls.deleted is null and Lsc.deleted is null and Lst.deleted is null and Lcc.deleted is null
and Ldp.deleted is null and Lsp.deleted is null and  Lp.deleted is null and Lc.deleted is null and lce.deleted is null
and lsce.deleted is null and  lsco.deleted is null order by 1,3;

--- Product Data
Select  Lpf.Name Product_Family_Name , Lpg.Name Product_Group , Li.Ingredient Ingredient_Name, Ld.Sheet_Name Data_Sheet_Name,
Ld.Activation_Date Activate_Datasheet , --Current Date,              Other date
Ld.Core_Sheet, Ld.No_Local_Assessment Global_No_Local_Assessment, Llt.Labeled_Term Listed_Terms,
Ld.Include_Datasheet_Id Include, To_Char(Ld.Notes) Notes, To_Char(Lpf.Comments) Comments, Lp.Prod_Name Product_Name, 
Lp.Prod_Name_Abbrv Product_Abbreviation, Lpf.Name Product_Family_Name, Lpg.Name Product_Group, 
query1.Prod_ingred Ingredient_name, query1.Prod_Dose Dose, query1.Prod_unit Unit,
To_Char(Lp.Prod_Generic_Name) Generic_Name, Lf.Formulation Dosage_Formulation,Lp.Concentration Strength,
Ldu.Unit Unit, Lp.Ind_Llt, Lp.Model_No, Lp.Intl_Birth_Date, dev_intl_birth_date Dev_Intl_Birthdate,
Lm.Manu_Name Manufacturer, Lp.Drug_Code Who_Drug_Code, lp.drug_code company_code
, Lp.Device_Code Device_Code, Lp.Psur_Group_Name, Lp.Comments, lpl.LOT_NO lot_no, lpl.expiration_date expiration_date,
Ll.Trade_Name, Ll.Award_Date, Ll.Withdraw_Date, Lm.Manu_Name Market_Authorization_Holder,
Decode(Ll.Biologic,0,'No','yes') Biologic_Vaccine, Decode(ll.ACTIVE_MOIETY,0,'No','Yes') Nt_Auto_Scheduled ,
Decode (Ll.Single_Use,0,'No','Yes'), Decode(Ll.Otc_Product,0,'No','Yes') Otc_Product,
Lc.Country Authorization_Country, --Countries List
Llty.License_Type, Ld.Sheet_Name Data_Sheet_Name, Ll.Lic_Number License, Ll.Company_Item_No Company_Item_Number,
Ll.Pma, Ll.Nomenclature, Ll.Medical_Device_Info_Id, --Product Name   Hide
ll.ctpr_group_name, ll.comments
From Lm_Product_Group Lpg,Lm_Product_Family Lpf, Lm_Product Lp, (Select Lp1.Product_Id Prod_Id, Li1.Ingredient Prod_Ingred, Lpc1.Concentration Prod_Dose, Ldu1.Unit Prod_Unit From 
Lm_Product Lp1, Lm_Product_Concentrations Lpc1, Lm_Ingredients Li1, Lm_Dose_Units Ldu1 Where 
Lpc1.Product_Id(+)=Lp1.Product_Id And Lpc1.Ingredient_Id=Li1.Ingredient_Id And Lpc1.Conc_Unit_Id=Ldu1.Unit_Id 
And Lp1.Deleted Is Null And Lpc1.Deleted Is Null and Li1.deleted is null and Ldu1.deleted is null) query1, lm_product_lots lpl,
Lm_Ingredients Li, Lm_Pf_Ingredients Lpi, Lm_Datasheet Ld, Lm_Labeled_Terms Llt, Lm_Dose_Units Ldu, Lm_Formulation Lf, 
Lm_Manufacturer Lm,lm_license ll, lm_lic_products llp, lm_countries lc, lm_license_types llty
Where 
Lpg.Product_Group_Id (+)=Lpf.Product_Group_Id And Lp.FAMILY_ID=lpf.family_id and query1.Prod_id(+)=lp.product_id and lpl.product_id(+)=lp.product_id and
Ll.License_Id =Llp.License_Id And Llp.Product_Id (+)=Lp.Product_Id And 
Li.Ingredient_Id = Lpi.Ingredient_Id and Lpi.Family_Id (+)=Lpf.Family_Id And 
Llt.Datasheet_Id (+) =Ld.Datasheet_Id And Ld.Family_Id (+)=Lpf.Family_Id And 
Ldu.Unit_Id (+)=Lp.Conc_Unit_Id And Lf.Formulation_Id (+)=Lp.Formulation_Id
And Lm.Manufacturer_Id (+)=Lp.Manufacturer_Id And Ll.Country_Id=Lc.Country_Id 
and llty.license_type_ID (+)=ll.license_type_ID  and  Lpg.deleted is null 
and Lpf.deleted is null and  Lp.deleted is null and Li.deleted is null and Lpi.deleted is null and Ld.deleted is null and Llt.deleted is null and Ldu.deleted is null and Lf.deleted is null and 
 Lm.deleted is null and ll.deleted is null and llp.deleted is null and lc.deleted is null and llty.deleted is null  order by 1;
