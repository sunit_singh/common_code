---for getting product family name and product group name
select lp.NAME,
       (select lpg.NAME
          from lm_product_group lpg
         where lpg.PRODUCT_GROUP_ID = lp.PRODUCT_GROUP_ID) prod_grp_name
  from lm_product_family lp
 where lp.NAME like '%Salicylic Acid%';
---for getting product family name and product group name

---for getting ingredients name 
select distinct li.INGREDIENT
  from lm_pf_ingredients lpi, lm_ingredients li
 where li.INGREDIENT_ID = lpi.INGREDIENT_ID
   and lpi.FAMILY_ID = 100030;
---for getting ingredients name

---for getting datasheet name
select ld.SHEET_NAME,
       ld.ACTIVATION_DATE,
       ld.CORE_SHEET,
       ld.NO_LOCAL_ASSESSMENT,
       ld.INCLUDE_DATASHEET_ID,
       ld.NOTES,
       (select lpf.COMMENTS from lm_product_family lpf where lpf.FAMILY_ID = 100030) comments
  from lm_datasheet ld
 where ld.FAMILY_ID = 100030;
---for getting datasheet name

---for getting Listed Terms
select llt.LABELED_TERM
  from lm_labeled_terms llt
 where llt.DATASHEET_ID = 1; -- take the datasheet id from datasheet name query
---for getting Listed Terms 

select * from lm_product lp where lp.FAMILY_ID = 100030;
select * from lm_product_concentrations;

---for getting product details
select lp.PROD_NAME,
       lp.PROD_NAME_ABBRV,
       lpf.NAME,
       (select lpg.NAME
          from lm_product_group lpg
         where lpg.PRODUCT_GROUP_ID = lpf.PRODUCT_GROUP_ID) prod_grp_name,
       lp.PROD_GENERIC_NAME,
       Lp.Ind_Llt,
       Lp.Model_No,
       Lp.Intl_Birth_Date,
       lp.DEV_INTL_BIRTH_DATE,
       (select lm.MANU_NAME from Lm_Manufacturer lm where lm.MANUFACTURER_ID = lp.MANUFACTURER_ID) manu_name,
       Lp.Drug_Code Who_Drug_Code,
       lp.drug_code company_code,
       Lp.Device_Code Device_Code,
       Lp.Psur_Group_Name,
       Lp.Comments,
       (select lf.FORMULATION from lm_formulation lf where lf.FORMULATION_ID = lp.FORMULATION_ID) formulation,
       lp.CONCENTRATION,
       (select ldu.UNIT from lm_dose_units ldu where ldu.UNIT_ID = lp.CONC_UNIT_ID) prod_dose_unit
  from lm_product lp, lm_product_family lpf
 where lp.FAMILY_ID = lpf.FAMILY_ID
   and lpf.FAMILY_ID = 100030;
---for getting product details
   
---for getting ingredients details   
select li.INGREDIENT,
       lpc.CONCENTRATION,
       (select ldu.UNIT
          from lm_dose_units ldu
         where ldu.UNIT_ID = lpc.CONC_UNIT_ID) dos_unit
  from lm_product lp, lm_product_concentrations lpc, lm_ingredients li
 where lp.PRODUCT_ID = lpc.PRODUCT_ID
   and lpc.INGREDIENT_ID = li.INGREDIENT_ID
   and lp.FAMILY_ID = 100030
   and lp.PRODUCT_ID = 1; --pass product coming from product details query;
---for getting ingredients details 

---for getting lot number details

select lpl.LOT_NO,lpl.EXPIRATION_DATE
  from lm_product lp, lm_product_lots lpl
 where lp.PRODUCT_ID = lpl.PRODUCT_ID
   and lp.FAMILY_ID = 100030
   and lp.PRODUCT_ID = 1;--pass product coming from product details query;
---for getting lot number details

---for getting license details
select ll.TRADE_NAME,
       ll.AWARD_DATE,
       ll.WITHDRAW_DATE,
       lm.MANU_NAME,
       Decode(Ll.Biologic, 0, 'No', 'yes') Biologic_Vaccine,
       Decode(ll.ACTIVE_MOIETY, 0, 'No', 'Yes') Nt_Auto_Scheduled,
       Decode(Ll.Single_Use, 0, 'No', 'Yes') single_use,
       Decode(Ll.Otc_Product, 0, 'No', 'Yes') Otc_Product,
       (select lc.COUNTRY from lm_countries lc where lc.COUNTRY_ID = ll.COUNTRY_ID) country,
       (select llt.LICENSE_TYPE from lm_license_types llt where llt.LICENSE_TYPE_ID = ll.LICENSE_TYPE_ID) license_type,
       (select ld.SHEET_NAME from lm_datasheet ld where ld.DATASHEET_ID = ll.DATASHEET_ID) datasheet_name,
       ll.COMPANY_ITEM_NO,
       ll.LIC_NUMBER,
       ll.APP_TYPE,
       ll.PMA,
       ll.NOMENCLATURE,
       ll.MEDICAL_DEVICE_INFO_ID,
       ll.CTPR_GROUP_NAME,
       ll.COMMENTS
  from lm_lic_products llp, lm_license ll, lm_manufacturer lm
 where llp.LICENSE_ID = ll.LICENSE_ID
   and ll.MANUFACTURER_ID = lm.MANUFACTURER_ID;
   and llp.PRODUCT_ID = 1 --- pass the product id coming from product details query
---for getting license details

select * from lm_pf_ingredients lpi where lpi.FAMILY_ID = 100030;

select * from lm_labeled_terms llt where llt.DATASHEET_ID in (100098,
100099
) 
select * from lm_datasheet;
select * from lm_product_lots

select * from lm_license;
select * from lm_lic_products;
select * from lm_license_types;
select * from lm_formulation;
select * from lm_product
